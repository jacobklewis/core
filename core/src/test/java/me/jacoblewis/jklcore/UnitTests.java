package me.jacoblewis.jklcore;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class UnitTests {
    @Test
    public void getNumberFormatTest() throws Exception {
        assertEquals("123", Tools.getInstance(null).getNumberFormat("123"));
        assertEquals("1,234", Tools.getInstance(null).getNumberFormat("1234"));
        assertEquals("12,345", Tools.getInstance(null).getNumberFormat("12345"));
        assertEquals("123,456", Tools.getInstance(null).getNumberFormat("123456"));
        assertEquals("1,234,567", Tools.getInstance(null).getNumberFormat("1234567"));
    }
}