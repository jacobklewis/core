package me.jacoblewis.jklcore.messenger;

import java.util.List;
import java.util.Locale;

/**
 * Created by LEJ4MTP on 9/15/2017.
 */

public class Message<T> {
    T message;
    List<Object> tags;

    public Message(T message, List<Object> tags) {
        this.message = message;
        this.tags = tags;
    }

    public T getMessage() {
        return message;
    }

    public List<Object> getTags() {
        return tags;
    }

    /**
     * Check if message has all of the following tags
     * @param tags The tags to check
     * @return true if all tags are in message
     */
    public boolean hasAll(Object ...tags) {
        for (Object obj : tags){
            if (this.tags.indexOf(obj)==-1)
                return false;
        }
        return true;
    }

    /**
     * Check if message has some of the following tags
     * @param tags The tags to check
     * @return true if some tags are in message
     */
    public boolean hasSome(Object ...tags) {
        for (Object obj : tags){
            if (this.tags.indexOf(obj)!=-1)
                return true;
        }
        return false;
    }

    /**
     * Check if message has strictly the tags specified in order
     * @param tags The tags to check (in order)
     * @return true if all tags are in the same order as message tags
     */
    public boolean is(Object ...tags) {
        if (tags.length != this.tags.size())
            return false;
        for (int i = 0; i < tags.length; i++) {
            if (!this.tags.get(i).equals(tags[i]))
                return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return String.format(Locale.ENGLISH, "%s : %s", message.toString(), tags.toString());
    }
}
