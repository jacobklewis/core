package me.jacoblewis.jklcore.messenger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.subjects.PublishSubject;


/**
 * Created by LEJ4MTP on 9/15/2017.
 */

public class Messenger {
    private static Messenger instance = new Messenger();
    private PublishSubject<Message<Object>> messagePublishSubject = PublishSubject.create();
    private Map<List<Object>, Disposable> disposableMap = new HashMap<>();

    public static int currentlySubscribed() {
        return instance.disposableMap.size();
    }

    public static <T> void post(T data, Object ...tags) {
        instance.messagePublishSubject.onNext(new Message<Object>(data, Arrays.asList(tags)));
    }

    public static <T> void subscribe(final OnMessageReceived<T> delegate) {
        instance.disposableMap.put(getKey(delegate), instance.messagePublishSubject.subscribe(new Consumer<Message<Object>>() {
            @Override
            public void accept(Message<Object> objectMessage) throws Exception {
                delegate.received((Message<T>) objectMessage);
            }
        }));
    }

    public static <T> void subscribeOn(final OnMessageReceived<T> delegate, final Object ...is) {
        System.out.println(getKey(delegate, is).toString());
        instance.disposableMap.put(getKey(delegate, is), instance.messagePublishSubject.subscribe(new Consumer<Message<Object>>() {
            @Override
            public void accept(Message<Object> objectMessage) throws Exception {
                if (objectMessage.is(is))
                    delegate.received((Message<T>) objectMessage);
            }
        }));
    }

    public static <T> void subscribeFor(final OnMessageReceived<T> delegate, final Object ...hasSome) {
        instance.disposableMap.put(getKey(delegate, hasSome), instance.messagePublishSubject.subscribe(new Consumer<Message<Object>>() {
            @Override
            public void accept(Message<Object> objectMessage) throws Exception {
                if (objectMessage.hasSome(hasSome))
                    delegate.received((Message<T>) objectMessage);
            }
        }));
    }

    public static <T> void unsubscribe(OnMessageReceived<T> delegate, Object ...attrs) {
        if (instance.disposableMap.containsKey(getKey(delegate, attrs))) {
            instance.disposableMap.get(getKey(delegate, attrs)).dispose();
            instance.disposableMap.remove(getKey(delegate, attrs));
        }
    }

    public static void unsubscribeAll() {
        for (Disposable d : instance.disposableMap.values()) {
            d.dispose();
        }
        instance.disposableMap.clear();
    }

    private static <T> List<Object> getKey(OnMessageReceived<T> delegate, Object ...objs) {
        List<Object> objects = new ArrayList<>(Arrays.asList(objs));
        objects.add(delegate);
        return objects;
    }

}
