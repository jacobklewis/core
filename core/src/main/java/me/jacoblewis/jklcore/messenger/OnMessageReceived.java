package me.jacoblewis.jklcore.messenger;

/**
 * Created by LEJ4MTP on 9/15/2017.
 */

public interface OnMessageReceived<T> {
    void received(Message<T> message);
}
