package me.jacoblewis.jklcore.database.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by Jacob on 5/5/2017.
 */

@Retention(RetentionPolicy.RUNTIME)
public @interface SQLObj {
    boolean isPrimary() default false;
    boolean isAutoInc() default false;
    String defaultVal() default "";
    String name() default "";
    int version() default -1;
}
