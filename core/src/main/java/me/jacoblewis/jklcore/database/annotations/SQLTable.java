package me.jacoblewis.jklcore.database.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by Jacob on 5/5/2017.
 */

@Retention(RetentionPolicy.RUNTIME)
public @interface SQLTable {
    String name() default "";
    String[] oldNames() default {};
    int[] oldVersions() default {};
}