package me.jacoblewis.jklcore.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import me.jacoblewis.jklcore.database.annotations.SQLObj;
import me.jacoblewis.jklcore.database.annotations.SQLTable;


/**
 * Created by Jacob on 4/30/2017.
 */

public class JLDataBase {

    private static int VERSION = 0;

    private DatabaseHelper dbHelper;
    private SQLiteDatabase db;

    public static ArrayList<DBTable> setTables(Class...classes) {
        ArrayList<DBTable> tables = new ArrayList<>();
        for (Class aClass:classes) {
            tables.add(new DBTable(aClass));
        }
        return tables;
    }

    public JLDataBase(Context context, String name, int version, ArrayList<DBTable> tables) {
        VERSION = version;
        dbHelper = new DatabaseHelper(context, name, version, tables);
    }

    public JLDataBase open() {
        db = dbHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        dbHelper.close();
        db = null;
    }

    // Use the below Methods

    public synchronized <T> Boolean insertObj(T obj){
        return insertObj(obj, -1);
    }
    private synchronized <T> Boolean insertObj(T obj, int version) {
        ArrayList<T> objs = new ArrayList<>();
        objs.add(obj);
        return insertObjs(objs, version);
    }
    public synchronized <T> Boolean insertObjs(final List<T> objs, final int version) {
        open();
        int v2;
        if (version==-1)
            v2 = db.getVersion();
        else
            v2 = version;

        DBTable table = dbHelper.getTable(objs.get(0).getClass());
        Boolean result = table != null;
        if (result) {
            for (T obj : objs) {
                if (-1 == db.insertOrThrow(table.getName(v2), null, table.getContents(obj, v2)))
                    result = false;
            }
        }
        close();
        return result;
    }

    public synchronized <T> Boolean updateObj(T obj) {
        return updateObj(obj, -1);
    }
    private synchronized <T> Boolean updateObj(T obj, int version) {
        ArrayList<T> objs = new ArrayList<>();
        objs.add(obj);
        return updateObjs(objs, version);
    }
    public synchronized <T> Boolean updateObjs(final ArrayList<T> objs, final int version) {
        open();
        int v2;
        if (version==-1)
            v2 = db.getVersion();
        else
            v2 = version;

        DBTable table = dbHelper.getTable(objs.get(0).getClass());
        Boolean result = table != null;
        if (result) {
            for (T obj : objs) {
                if (-1 == db.update(table.getName(v2), table.getContents(obj, v2),
                        table.getPrimaryName() + "=" + table.getPrimaryKey(obj), null))
                    result = false;
            }
        }
        close();
        return result;
    }

    public synchronized Boolean updateRaw(final Class obj, final ContentValues values, final String whereClause) {
        open();
        DBTable table = dbHelper.getTable(obj);

        Boolean result = table != null && -1 != db.update(table.getName(db.getVersion()), values, whereClause, null);
        close();
        return result;
    }

    public synchronized Boolean deleteRaw(final Class obj, final String whereClause) {
        open();
        DBTable table = dbHelper.getTable(obj);

        Boolean result = table != null && -1 != db.delete(table.getName(db.getVersion()), whereClause, null);
        close();
        return result;
    }

    public synchronized <T> ArrayList<T> selectObjs(final Class<T> objClass, final DBOptions options) {

        DBOptions opts;
        if (options == null)
            opts = new DBOptions();
        else
            opts = options;
        open();
        DBTable table = dbHelper.getTable(objClass);
        int version = opts.getDbVersion();
        if (version == -1) version = db.getVersion();
        String keys[] = table.getKeys(version);
        Cursor data = db.query(table.getName(version), keys,
                opts.getWhere(),null,null,null, opts.getOrder());
        ArrayList<T> objs = new ArrayList<>();

        if(data.moveToFirst()) {
            do {
                try {
                    T obj = objClass.newInstance();
                    table.injectCursor(obj, data, version);
                    objs.add(obj);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            } while (data.moveToNext());
        }
        close();
        return objs;
    }
    public synchronized <T> ArrayList<T> selectObjs(Class<T> objClass) {
        return selectObjs(objClass, null);
    }

    // Use the above Methods

    private static class DatabaseHelper extends SQLiteOpenHelper {
        private ArrayList<DBTable> tables;
        public DatabaseHelper(Context context, String name, int version, ArrayList<DBTable> tables) {
            super(context, name, null, version);
            this.tables = tables;
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            for (DBTable table:tables) {
                Log.e("DB 2", String.valueOf(VERSION));
                db.execSQL(table.getCreate(VERSION));
            }
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            for (DBTable table:tables) {
                db.execSQL(table.getTempAlter(oldVersion, newVersion)); // Create temp Table
                db.execSQL(table.getCreate(newVersion)); // Create new Table
                db.execSQL(table.getRepopulate(oldVersion, newVersion)); // Fill new Table with old data
                db.execSQL(table.getDropTemp(newVersion)); // Drop old Table
            }

        }

        public DBTable getTable(Class obj) {
            for (DBTable table:tables) {
                if (table.getObjClass() == obj) return table;
            }
            return null;
        }
    }

    private static class DBTable {
        public Class objClass;
        private enum JAVA_TYPES {BOOLEAN, INTEGER, LONG, FLOAT, DOUBLE, STRING}

        public DBTable(Class objClass) {
            this.objClass = objClass;
        }

        public String getName(boolean temp, int version) {
            String name = (temp?"tempdb_":"")+objClass.getName();
            if (objClass.isAnnotationPresent(SQLTable.class)) {
                SQLTable table = (SQLTable) objClass.getAnnotation(SQLTable.class);
                name = (temp?"tempdb_":"") + table.name();
                for (int i = 0; i < table.oldVersions().length; i++) {
                    if(version <= table.oldVersions()[i])
                        name = table.oldNames()[i];
                }
            }
            return "`"+name+"`";
        }

        public String getName(int version) {
            return getName(false, version);
        }

        public Class getObjClass(){
            return objClass;
        }

        public <T> int getPrimaryKey(T object) {
            for (Field field : objClass.getFields()) {
                // Get Annotations
                if (field.isAnnotationPresent(SQLObj.class)) {
                    SQLObj sqlDef = field.getAnnotation(SQLObj.class);
                    if (sqlDef.isPrimary()) {
                        try {
                            return field.getInt(object);
                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            return -1;
        }

        public String getPrimaryName(){
            for (Field field : objClass.getFields()) {
                String name = field.getName();

                // Get Annotations
                if (field.isAnnotationPresent(SQLObj.class)) {
                    SQLObj sqlDef = field.getAnnotation(SQLObj.class);
                    if (!sqlDef.name().equals(""))
                        name = sqlDef.name();
                    if (sqlDef.isPrimary())
                        return name;
                }
            }
            return null;
        }

        public <T> ContentValues getContents(T object, int version) {
            ContentValues contentValues = new ContentValues();
            String defaultVal = "";
            for (Field field : objClass.getFields()) {
                String name = field.getName();
                String type = getDBType(field);
                if (field.isAnnotationPresent(SQLObj.class)) {
                    // Skip field if version is too old
                    if (field.getAnnotation(SQLObj.class).version() > version) continue;
                    SQLObj sqlDef = field.getAnnotation(SQLObj.class);
                    if (!sqlDef.name().equals(""))
                        name = sqlDef.name();
                    // Skip over Auto Increment Keys
                    try {
                        if (sqlDef.isAutoInc() && (field.get(object)==null || (int)field.get(object)==-1)) continue;
                    } catch (IllegalAccessException e) {e.printStackTrace();}

                    defaultVal = sqlDef.defaultVal();

                    try {
                        // Skip over fields that are not filled in and have a default
                        if (field.get(object) == null && !defaultVal.equals(""))
                            continue;

                        if (type.equals("INTEGER"))
                            contentValues.put(name, field.getInt(object));
                        if (type.equals("NUMERIC"))
                            contentValues.put(name, field.getDouble(object));
                        if (type.equals("TEXT"))
                            contentValues.put(name, (String) field.get(object));
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
            }
            return contentValues;
        }

        public String getCreate(int version) {
            String output = "CREATE TABLE " + getName(version) + " (";
            for (Field field : objClass.getFields()) {
                String defaultQuery = "";
                String additionalFields = "";
                String name = field.getName();

                // Get Annotations
                if (field.isAnnotationPresent(SQLObj.class)) {
                    SQLObj sqlDef = field.getAnnotation(SQLObj.class);
                    if (!sqlDef.defaultVal().equals(""))
                        defaultQuery = " DEFAULT " + sqlDef.defaultVal();
                    if (!sqlDef.name().equals(""))
                        name = sqlDef.name();
                    if (sqlDef.isPrimary())
                        additionalFields += " PRIMARY KEY";
                    if (sqlDef.isAutoInc())
                        additionalFields += " AUTOINCREMENT";

                    output += name + " ";
                    output += getDBType(field) + additionalFields + defaultQuery + ", ";
                }
            }
            return output.substring(0, output.length()-2) + ");";
        }

        public String getTempAlter(int old_version, int new_version) {
            return "ALTER TABLE " + getName(old_version) + " RENAME TO " + getName(true, new_version) + ";";
        }

        public String getRepopulate(int old_version, int new_version) {
//            String[] keys = getKeys(new_version);
//            StringBuilder keyStr = new StringBuilder();
//            for (String key:keys) {
//                keyStr.append(key).append(", ");
//            }
//            keyStr = new StringBuilder(keyStr.substring(0, keyStr.length() - 2));

            String[] keys2 = getKeys(old_version);
            StringBuilder keyStr2 = new StringBuilder();
            for (String key:keys2) {
                keyStr2.append(key).append(", ");
            }
            keyStr2 = new StringBuilder(keyStr2.substring(0, keyStr2.length() - 2));
            return "INSERT INTO " + getName(new_version) + " ("+ keyStr2 + ") SELECT " + keyStr2 + " FROM " + getName(true, old_version) + ";";
        }

        public String getDropTemp(int version) {
            return "DROP TABLE " + getName(true, version) + ";";
        }

        public String[] getKeys(int version) {
            ArrayList<String> keys = new ArrayList<>();
            for (int i = 0; i < objClass.getFields().length; i++) {
                Field field = objClass.getFields()[i];
                String name = field.getName();
                // Get Annotations
                if (field.isAnnotationPresent(SQLObj.class)) {
                    // Skip field if version is too old
                    if (field.getAnnotation(SQLObj.class).version() > version) continue;
                    SQLObj sqlDef = field.getAnnotation(SQLObj.class);
                    if (!sqlDef.name().equals(""))
                        name = sqlDef.name();
                    keys.add(name);
                }
            }
            return convertToStringArray(keys);
        }

        private static String getDBType(Field field) {
            Class c = field.getType();
            if (	c == int.class || c == Integer.class ||
                    c == boolean.class || c == Boolean.class)
                return "INTEGER";
            else if (	c == double.class || c == Double.class ||
                    c == float.class || c == Float.class ||
                    c == long.class || c == Long.class)
                return "NUMERIC";
            else if (	c == String.class)
                return "TEXT";
            return "NONE";
        }

        private static JAVA_TYPES getJavaType(Field field) {
            Class[] classes = { boolean.class, Boolean.class,
                                int.class, Integer.class,
                                long.class, Long.class,
                                float.class, Float.class,
                                double.class, Double.class,
                                String.class};
            switch (Arrays.asList(classes).indexOf(field.getType())) {
                case 0:case 1:
                    return JAVA_TYPES.BOOLEAN;
                case 2:case 3:
                    return JAVA_TYPES.INTEGER;
                case 4:case 5:
                    return JAVA_TYPES.LONG;
                case 6:case 7:
                    return JAVA_TYPES.FLOAT;
                case 8:case 9:
                    return JAVA_TYPES.DOUBLE;
                default:
                    return JAVA_TYPES.STRING;
            }
        }

        public <T> void injectCursor(T obj, Cursor cursor, int version) {
            for (int i = 0; i < objClass.getFields().length; i++) {
                Field field = objClass.getFields()[i];
                String name = field.getName();
                // Get Annotations
                if (field.isAnnotationPresent(SQLObj.class)) {
                    // Skip field if version is too old
                    if (field.getAnnotation(SQLObj.class).version() > version) continue;
                    SQLObj sqlDef = field.getAnnotation(SQLObj.class);
                    if (!sqlDef.name().equals(""))
                        name = sqlDef.name();
                }else continue;
                try {
                    switch (getJavaType(field)) {
                        case BOOLEAN:
                            field.setBoolean(obj, 0 < cursor.getInt(cursor.getColumnIndex(name)));
                            break;
                        case INTEGER:
                            field.setInt(obj, cursor.getInt(cursor.getColumnIndex(name)));
                            break;
                        case FLOAT:
                            field.setFloat(obj, cursor.getFloat(cursor.getColumnIndex(name)));
                            break;
                        case DOUBLE:
                            field.setDouble(obj, cursor.getDouble(cursor.getColumnIndex(name)));
                            break;
                        case STRING:
                            field.set(obj, cursor.getString(cursor.getColumnIndex(name)));
                            break;
                        default:
                            break;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static class DBOptions {
        private String whereStr = "";
        private int dbVersion;
        private DBOrder[] orders;

        public DBOptions(int dbVersion, String whereStr, DBOrder ...orders) {
            this.dbVersion = dbVersion;
            this.whereStr = whereStr;
            this.orders = orders;
        }
        public DBOptions(String whereStr, DBOrder ...orders) {
            this(-1, whereStr, orders);
        }

        public DBOptions(String whereStr) {
            this(-1, whereStr);
        }

        public DBOptions(DBOrder ...orders) {
            this(-1, "", orders);
        }

        public DBOptions() {
            this(-1, "");
        }

        public int getDbVersion() {
            return dbVersion;
        }

        public String getWhere() {
            return whereStr;
        }

        public String getOrder() {
            String orderStr = "  ";
            for (DBOrder order:orders) {
                orderStr += order.order + ", ";
            }
            return orderStr.substring(0, orderStr.length()-2);
        }
    }

    public static class DBOrder{
        public String order;
        public DBOrder(String col, boolean desc) {
            order = col + (desc?" DESC":" ASC");
        }
    }

    // Helpers
    private static String[] convertToStringArray(ArrayList<String> arrayList) {
        String temp[] = new String[arrayList.size()];
        for (int i = 0; i < arrayList.size(); i++) {
            temp[i] = arrayList.get(i);
        }
        return temp;
    }

}
