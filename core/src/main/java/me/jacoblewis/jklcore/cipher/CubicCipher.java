/**
 * 
 */
package me.jacoblewis.jklcore.cipher;

import java.util.Locale;

/**
 * @author Jacob Lewis
 *
 */
public class CubicCipher {

	public CubicCipher(){};
	public CubicCipher(String key){
		init(key);
	};
    public CubicCipher(String originalKey, String newKey){
        init(originalKey,newKey);
    }
	
	public void init(String key)
	{
		cKey = key;
	}
    public void init(String originalKey, String newKey){
        cKey = originalKey;
        nKey = newKey;
    }
	
	/**
	 * ENCODE a string
	 * @param data holds string data
	 */
	public void encode(String data)
	{
        try {
            String secData = data;
            for (int i = 0; i < cKey.length(); i++) {
                String tempDat = "";
                for (int j = 0; j < data.length(); j++) {
                    int tmp = (int) (secData.charAt(j) + Math.pow(cKey.charAt(i) + j, 3) + cKey.charAt(i) * i);
                    while (tmp > 255)
                        tmp -= 256;
                    tempDat += (char) tmp;// + " ";
                }
                secData = tempDat;
            }

            cData = "";
            for (int j = 0; j < secData.length(); j++) {
                cData += Hexicode.toHex((int) secData.charAt(j));
            }
        }catch (Exception e) {
            e.printStackTrace();
        }
	}
	
	public String encodeNow(String data)
	{
		encode(data);
		return getData();
	}
	
	/**
	 * DECODE an encoded string
	 * @return decoded string
	 */
	public String decode(String code)
	{

		String secData = "";
        try {
            for (int i = 0; i < code.length(); i += 2) {
                secData += (char) Hexicode.toDec(code.substring(i, i + 2).toUpperCase(Locale.ENGLISH));
            }

            for (int i = cKey.length() - 1; i >= 0; i--) {
                String tempDat = "";
                for (int j = secData.length() - 1; j >= 0; j--) {
                    int tmp = (int) (secData.charAt(j) - Math.pow(cKey.charAt(i) + j, 3) - cKey.charAt(i) * i);
                    while (tmp < 0)
                        tmp += 256;
                    tempDat += (char) tmp;// + " ";
                }
                secData = new StringBuilder(tempDat).reverse().toString();
            }
        }catch (Exception e) {
            e.printStackTrace();
        }
		
		return secData;
	}

    public String reEncode(String data)
    {
        String tVal = decode(data);
        String ck = cKey;
        cKey = nKey;
        String finalVal = encodeNow(tVal);
        cKey = ck;
        return finalVal;
    }

	/**
	 * getData()
	 * -returns encrypted data
	 */
	public String getData()
	{
		return cData;
	}



    public static String qEncode(String key, String data)
    {
        CubicCipher c = new CubicCipher(key);
        return c.encodeNow(data);
    }
    public static String qDecode(String key, String data)
    {
        CubicCipher c = new CubicCipher(key);
        return c.decode(data);
    }
	
	
	
	private String cKey;
    private String nKey;
	private String cData = "";
}