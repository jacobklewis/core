/**
 * 
 */
package me.jacoblewis.jklcore.cipher;

/**
 * @author Admin
 *
 */
public class Hexicode {
	public static String toHex(int val)
	{
		String str = "";
		int first = val / 16;
		int second = val % 16;
		
		str += (char)codes[first]+""+(char)codes[second];
		
		/*System.out.println(first);
		System.out.println(second);
		*/
		return str;
	}
	public static int toDec(String hex)
	{
		int num = 0;
		int first = charLoc( codes, hex.charAt(0) )*16;
		int second = charLoc( codes, hex.charAt(1) );
		
		num = first + second;
		return num;
	}
	private static int charLoc(char src[], char val)
	{
		for (int i = 0; i < src.length; i++) {
			if(src[i] == val)
				return i;
		}
		return -1;
	}
	
	private static char codes[] = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
}
