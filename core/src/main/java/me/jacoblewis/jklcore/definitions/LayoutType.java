package me.jacoblewis.jklcore.definitions;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by Jacob on 9/23/2017.
 */

@IntDef({
        LayoutType.PHONE,
        LayoutType.TABLET
})
@Retention(RetentionPolicy.SOURCE)
public @interface LayoutType {
    int PHONE = 0;
    int TABLET = 1;
}
