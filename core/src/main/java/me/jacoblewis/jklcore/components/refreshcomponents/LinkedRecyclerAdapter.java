package me.jacoblewis.jklcore.components.refreshcomponents;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import java.util.ArrayList;

import me.jacoblewis.jklcore.R;


/**
 * Created by Explor on 6/1/2015.
 */
public class LinkedRecyclerAdapter<J,T> extends RecyclerView.Adapter<LinkedRecyclerViewHolder> {
    public ArrayList<J> itemList;
    private T mc;
    private boolean mAllEnabled = true;
    private int lastPosition = -1;
    private Context context;

    public LinkedRecyclerAdapter(Context context, T mainCall) {
        super();
        mc = mainCall;
        itemList = new ArrayList<>();
        this.context = context;
    }

    @Override
    public LinkedRecyclerViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        return null; // OVERRIDE
    }

    @Override
    public int getItemViewType(int position) {
        // OVERRIDE
        return 0;
    }

    public void setAllItemsEnabled(boolean enable){
        mAllEnabled = enable;
        notifyItemRangeChanged(0, getItemCount());
    }

    public ArrayList<J> getItemList() {
        return itemList;
    }

    public void newItems(ArrayList<J> items) {
        itemList.clear();
        updateItems(items);
    }

    public void removeAllItems() {
        while(!itemList.isEmpty()) {
            removeItem(0);
        }
    }

    public void updateItems(ArrayList<J> items) {

        ArrayList< Integer > addedItems = new ArrayList<>();
        ArrayList< Integer > removedItems = new ArrayList<>();

        for (int i = 0; i < items.size(); i++) {

            if ( itemList.size() <= i ) {
                addedItems.add(i);
            }
        }

        for (int i = items.size(); i < itemList.size(); i++) {
            removedItems.add(i);
        }

        itemList = new ArrayList<>(items);
        // TODO: notify
        for (Integer i : addedItems)
            notifyItemInserted(i);
        for (Integer k : removedItems)
            notifyItemChanged(k);

    }

    public void addItems(J ...items) {
        for (J a:items) {
            addItem(a);
        }
    }
    public void addItems(ArrayList<J> items) {
        for (J a:items) {
            addItem(a);
        }
    }

    public void addItem(J item) {
        addItem(item,itemList.size());
    }

    public void addItem(J item, int pos) {
        itemList.add(pos, item);
        notifyItemInserted(itemList.size()-1);
    }

    public void removeItem(int pos) {
        itemList.remove(pos);
        notifyItemRemoved(pos);
    }

    @Override
    public void onBindViewHolder(LinkedRecyclerViewHolder linkedRecyclerViewHolder, int i) {
        linkedRecyclerViewHolder.itemView.setEnabled(mAllEnabled);
        linkedRecyclerViewHolder.bindItem(itemList.get(i),i,mc);

        setAnimation(linkedRecyclerViewHolder.itemView, i);
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    /**
     * Here is the key method to apply the animation
     */
    private void setAnimation(View viewToAnimate, int position)
    {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition)
        {
            Animation animation = AnimationUtils.loadAnimation(context, R.anim.element_alpha_in);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }
}
