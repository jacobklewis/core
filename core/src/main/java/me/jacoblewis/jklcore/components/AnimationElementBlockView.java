package me.jacoblewis.jklcore.components;

import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by Exper on 7/13/2014.
 */
public class AnimationElementBlockView extends View {
    protected ValueAnimator animTime1;
    protected ValueAnimator animTime2;
    protected ValueAnimator animTime3;
    protected ValueAnimator animTime4;
    protected ValueAnimator animTime5;

    protected Paint mPaint[] = {new Paint(Paint.ANTI_ALIAS_FLAG),new Paint(Paint.ANTI_ALIAS_FLAG),new Paint(Paint.ANTI_ALIAS_FLAG)};
    protected Paint mPaintSec[] = {new Paint(Paint.ANTI_ALIAS_FLAG),new Paint(Paint.ANTI_ALIAS_FLAG),new Paint(Paint.ANTI_ALIAS_FLAG)};
    protected Paint mPaintBlack = new Paint(Paint.ANTI_ALIAS_FLAG);
    protected Paint mPaintWhite = new Paint(Paint.ANTI_ALIAS_FLAG);
    protected RectF mBoundsF;
    protected int realWidth;
    protected int realHeight;
    private boolean isInset = false;
    private boolean hidden = false;

    private float density;

    public AnimationElementBlockView(Context context) {
        this(context, null);
    }

    public AnimationElementBlockView(Context context, AttributeSet attrs) {
        super(context, attrs);


        density = context.getResources().getDisplayMetrics().density;

        int c1 = Color.parseColor("#1a1a1a");
        int c2 = Color.parseColor("#fafafa");

        mPaintBlack.setColor(c1);
        mPaintWhite.setColor(c2);

        setColors(new int[]{c1,c1,c1},new int[]{c1,c2,c2});
    }

    public void setColors(int[] priColor, int[] secColor) {
        for (int i = 0; i < 3; i++) {
            mPaint[i].setColor(priColor[i]);
            mPaintSec[i].setColor(secColor[i]);
        }
        requestLayout();
        invalidate();
    }
    public void setMainColor(int priColor) {
        mPaint[0].setColor(priColor);
        requestLayout();
        invalidate();
    }

    public void setLineThickness(float thickness) {

        for (int i = 0; i < 3; i++) {
            mPaint[i].setStrokeWidth(thickness);
            mPaintSec[i].setStrokeWidth(thickness);
        }
    }

    protected int getRealPix(float pix) {
        return (int)(pix*density);
    }

    protected RectF getBounds() {
        mBoundsF = new RectF(0,0,getWidth(),getHeight());
        final int halfBorder = (int) (mPaint[0].getStrokeWidth()/2f + 0.5f);
        if(isInset)
            mBoundsF.inset(halfBorder, halfBorder);
        return mBoundsF;
    }

    protected void setInset(boolean insetVal) {
        isInset = insetVal;
    }

    public boolean isHidden() {
        return hidden;
    }
    public void setHidden(boolean h) {
        hidden = h;
    }
}
