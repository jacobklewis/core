package me.jacoblewis.jklcore.components;

import android.view.animation.Interpolator;

/**
 * Created by Jacob on 9/16/2017.
 */

public class SwiftInOut implements Interpolator {
    public SwiftInOut(){}
    @Override
    public float getInterpolation(float v) {
        return 1-(float) Math.pow(Math.cos(Math.PI*v)/2+0.5,3*v);
    }
}
