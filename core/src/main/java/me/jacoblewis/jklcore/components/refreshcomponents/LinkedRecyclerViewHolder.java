package me.jacoblewis.jklcore.components.refreshcomponents;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by Explor on 6/1/2015.
 */
public class LinkedRecyclerViewHolder<J,T> extends RecyclerView.ViewHolder implements View.OnLongClickListener,View.OnClickListener {
    protected T mc;
    protected J item;
    protected int position;

    public LinkedRecyclerViewHolder(View itemView) {
        super(itemView);
    }

    public void bindItem(J currentItem,int pos,T mainCalls) {
        mc = mainCalls;
        item = currentItem;
        position = pos;
        setUpView();
        itemView.setOnClickListener(this);
        itemView.setOnLongClickListener(this);
        // OVERRIDE
    }

    protected void setUpView() {
        // OVERRIDE
    }

    @Override
    public void onClick(View v) {
        // OVERRIDE
    }

    @Override
    public boolean onLongClick(View v) {
        // OVERRIDE
        return false;
    }
}