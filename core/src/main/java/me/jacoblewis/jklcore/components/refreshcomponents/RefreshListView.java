package me.jacoblewis.jklcore.components.refreshcomponents;

import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

/**
 * Created by Exper on 8/11/2014.
 */
public class RefreshListView extends RecyclerView{

    public RefreshListView(Context context) {
        this(context,null);
    }
    public RefreshListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setOnScrollListener(scrollListener);
    }
    public RefreshListView(Context context, AttributeSet attrs, int defStyle){
        super(context, attrs, defStyle);
        this.setOnScrollListener(scrollListener);
    }

    public void sdraw(Canvas canvas) {



//        for (int i=0; i<getChildCount(); i++) {
//            if(getChildAt(i)!=null && pullOut!=null)
//                getChildAt(i).setTranslationX(((Float)pullOut.getAnimatedValue())*(rad*2+padding*2));
//        }


    }



    private ValueAnimator.AnimatorUpdateListener pullOutUpdate = new ValueAnimator.AnimatorUpdateListener() {
        @Override
        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            //setPadding((int)(((Float)valueAnimator.getAnimatedValue())*100),0,0,0);
            invalidate();
        }
    };

    @Override
    public void setOnScrollListener(OnScrollListener listener) {
        super.setOnScrollListener(listener);
    }

    // TODO: think of alternative
    public void setLinearLabels() {
        //setAdapter(adapter);

        pullOut = ValueAnimator.ofFloat(0f,0f);
        pullOut.setDuration(10);
        pullOut.start();
        pullOut.addUpdateListener(pullOutUpdate);
    }

    OnScrollListener scrollListener = new OnScrollListener() {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            LinearLayoutManager lm = (LinearLayoutManager) recyclerView.getLayoutManager();
            int visibleCount = lm.getChildCount();
            int totalCount = lm.getItemCount();
            int totalPastItems = lm.findFirstVisibleItemPosition();
            if ((visibleCount+totalPastItems) >= totalCount) {
                if (!extended && expandable) {
                    extended = true;
                    onExtendList();
                }
            }
        }
    };

    protected void setExpandable(boolean extend) {
        expandable = extend;
    }
    protected void resetExtendList() {
        extended = false;
    }
    public void onExtendList() {
        // OVERRIDE
    }


    //Extensions
    private boolean extended = false, expandable = false;

    //Animators
    private ValueAnimator pullOut;

}
