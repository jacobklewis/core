package me.jacoblewis.jklcore.components.recyclerview;

import android.support.annotation.LayoutRes;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Explor on 6/1/2015.
 */
public abstract class RBRecyclerViewHolder<J, T> extends RecyclerView.ViewHolder implements View.OnLongClickListener,View.OnClickListener {
    private T callback;
    private J item;
    private int position;

    public RBRecyclerViewHolder(ViewGroup viewGroup,
                                @LayoutRes int viewHolderLayout) {

        super(LayoutInflater.from(viewGroup.getContext()).
                inflate(viewHolderLayout,viewGroup,false));
    }

    public void bindItem(J currentItem,int pos,T callback) {
        this.callback = callback;
        this.item = currentItem;
        position = pos;
        itemView.setOnClickListener(this);
        itemView.setOnLongClickListener(this);

        setUpView(itemView, item, position, callback);
    }

    protected abstract void setUpView(View itemView, J item, int position, T delegate);
    protected abstract void onClick(View itemView, J item, int position, T delegate);
    protected abstract void onLongClick(View itemView, J item, int position, T delegate);

    @Override
    public final void onClick(View v) {
        onClick(v, item, position, callback);
    }

    @Override
    public final boolean onLongClick(View v) {
        onLongClick(v, item, position, callback);
        return true;
    }
}