package me.jacoblewis.jklcore.components.recyclerview;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jacob Lewis on 6/1/2015.
 */
public abstract class RBRecyclerAdapter<TYPE, CALLBACK> extends RecyclerView.Adapter<RBRecyclerViewHolder> {
    public List<TYPE> itemList;
    private CALLBACK callback;
    private boolean mAllEnabled = true, animateIn = false;
    private int lastPosition = -1;
    private Context context;

    public RBRecyclerAdapter(Context context, CALLBACK delegate) {
        super();
        callback = delegate;
        itemList = new ArrayList<>();
        this.context = context;
    }

    @Override
    public abstract RBRecyclerViewHolder onCreateViewHolder(ViewGroup viewGroup, int type);

    @Override
    public abstract int getItemViewType(int position);

    public void setAllItemsEnabled(boolean enable){
        mAllEnabled = enable;
        notifyItemRangeChanged(0, getItemCount());
    }

    public List<TYPE> getItemList() {
        return itemList;
    }

    public void newItems(List<TYPE> items) {
        itemList.clear();
        updateItems(items);
    }

    public void removeAllItems() {
        while(!itemList.isEmpty()) {
            removeItem(0);
        }
    }

    public void updateItems(List<TYPE> items) {

        List< Integer > addedItems = new ArrayList<>();
        List< Integer > removedItems = new ArrayList<>();

        for (int i = 0; i < items.size(); i++) {

            if ( itemList.size() <= i ) {
                addedItems.add(i);
            }
        }

        for (int i = items.size(); i < itemList.size(); i++) {
            removedItems.add(i);
        }

        itemList = new ArrayList<>(items);
        // TODO: notify
        for (Integer i : addedItems)
            notifyItemInserted(i);
        for (Integer k : removedItems)
            notifyItemChanged(k);

    }

    public void addItems(TYPE...items) {
        for (TYPE a:items) {
            addItem(a);
        }
    }
    public void addItems(List<TYPE> items) {
        for (TYPE a:items) {
            addItem(a);
        }
    }

    public void addItem(TYPE item) {
        addItem(item,itemList.size());
    }

    public void addItem(TYPE item, int pos) {
        itemList.add(pos, item);
        notifyItemInserted(itemList.size()-1);
    }

    public void removeItem(int pos) {
        itemList.remove(pos);
        notifyItemRemoved(pos);
    }

    @Override
    public void onBindViewHolder(RBRecyclerViewHolder linkedRecyclerViewHolder, int i) {
        linkedRecyclerViewHolder.itemView.setEnabled(mAllEnabled);
        linkedRecyclerViewHolder.bindItem(itemList.get(i), i, callback);

        if (animateIn)
            setAnimation(linkedRecyclerViewHolder.itemView, i);
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    /**
     * Here is the key method to apply the animation
     */
    private void setAnimation(View viewToAnimate, int position)
    {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition)
        {
            viewToAnimate.setAlpha(0f);
            viewToAnimate.animate().alpha(1).setDuration(300).start();
            lastPosition = position;
        }
    }
}