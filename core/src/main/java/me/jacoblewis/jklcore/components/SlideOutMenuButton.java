package me.jacoblewis.jklcore.components;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by Exper on 7/26/2014.
 */
public class SlideOutMenuButton extends View {
    public SlideOutMenuButton(Context context) {
        super(context);
    }

    public SlideOutMenuButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        Paint p = new Paint(Paint.ANTI_ALIAS_FLAG);
        p.setColor(Color.parseColor("#fafafa"));


        int thickness = 10 * getPlusWidth() / 64;
        int offSetClose = (getWidth()-thickness)/2;
        int offSetFar = (getWidth()-getPlusWidth())/2;

        canvas.drawRect(offSetFar, offSetClose+thickness*2, offSetFar+getPlusWidth(), offSetClose+thickness*3, p);

        canvas.drawRect(offSetFar, offSetClose-thickness*2, offSetFar+getPlusWidth(), offSetClose-thickness, p);

        canvas.drawRect(offSetFar, offSetClose, offSetFar+getPlusWidth(), offSetClose+thickness, p);
    }

    private int getPlusWidth() {
        return getWidth()/3;
    }
}
