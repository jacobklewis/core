package me.jacoblewis.jklcore.date;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Exper on 7/11/2014.
 */
public class DateHolder implements Parcelable {
    public static String DATE_HOLDER = "date_holder";
    private Calendar now;
    public DateHolder(int year, int month, int day, int hour, int minute) {
        setDay(day);
        setMonth(month);
        setYear(year);
        setHour(hour);
        setMinute(minute);
        setAm(hour<12);
    }

    public DateHolder(Bundle bundle) {
        setDay(bundle.getInt("DAY"));
        setMonth(bundle.getInt("MONTH"));
        setYear(bundle.getInt("YEAR"));
        setHour(bundle.getInt("HOUR"));
        setMinute(bundle.getInt("MINUTE"));
        setMinute(bundle.getInt("SECOND"));
        setAm(bundle.getBoolean("AM"));
    }

    public DateHolder(String date_str, String format) {
        now = Calendar.getInstance();//2014-09-15 00:00:58:767
        SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.ENGLISH);
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss:SSS", Locale.ENGLISH);

        try {
            now.setTime(sdf.parse(date_str));
        } catch (ParseException e) {
            e.printStackTrace();
            try {
                now.setTime(sdf2.parse(date_str));
            } catch (ParseException e2) {
                e2.printStackTrace();
            }
        }
        setDay(now.get(Calendar.DAY_OF_MONTH));
        setMonth(now.get(Calendar.MONTH));
        setYear(now.get(Calendar.YEAR));

        setHour(now.get(Calendar.HOUR_OF_DAY));
        setMinute(now.get(Calendar.MINUTE));
        setAm(true);

        //Log.e("DATE STUFF RAW 1:", date_str);
        //Log.e("DATE STUFF RAW", now.get(Calendar.DAY_OF_MONTH)+"-"+now.get(Calendar.MONTH)+"-"+now.get(Calendar.YEAR)+" "+now.get(Calendar.HOUR_OF_DAY)+":"+Calendar.MINUTE);

        //Log.e("DATE STUFF", getYear()+"-"+(getMonth()<10?"0":"")+getMonth()+"-"+(getDay()<10?"0":"")+getDay()+" "+(getHour()<10?"0":"")+getHour()+":"+(getMinute()<10?"0":"")+getMinute()+":00.000");
    }

    public DateHolder(String date_str) {
        this(date_str, "MM/dd/yyyy hh:mm:ss a");
    }

    public DateHolder()
    {
        Calendar now = Calendar.getInstance();
        setDay(now.get(Calendar.DAY_OF_MONTH));
        setMonth(now.get(Calendar.MONTH));
        setYear(now.get(Calendar.YEAR));

        setHour(now.get(Calendar.HOUR_OF_DAY));
        setMinute(now.get(Calendar.MINUTE));
        setSecond(now.get(Calendar.SECOND));
    }

    public String printDate() {
        return (getMonth()+1)+"/"+getDay()+"/"+getYear()+" "+(getHour()<10?"0":"")+getHour()+":"+(getMinute()<10?"0":"")+getMinute()+":"+(getSecond()<10?"0":"")+getSecond();
    }


    public Calendar getCalendar() {
        return now;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        Bundle outState = new Bundle();
        outState.putInt("YEAR",year);
        outState.putInt("MONTH",month);
        outState.putInt("DAY",day);
        outState.putInt("HOUR",hour);
        outState.putInt("MINUTE",minute);
        outState.putInt("SECOND",second);
        outState.putBoolean("AM",am);

        parcel.writeBundle(outState);
    }

    public static String getMonth(int i){
        String dates[] = {"January","February","March","April","May","June","July","August","September","October","November","December"};
        return dates[i];
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    public int getSecond() {
        return second;
    }

    public void setSecond(int second) {
        this.second = second;
    }

    public boolean getAm() {
        return am;
    }

    public void setAm(boolean am) {
        this.am = am;
    }

    private int year;
    private int month;
    private int day;
    private int hour;
    private int minute;
    private int second;
    private boolean am;

    public static final Creator<DateHolder> CREATOR = new Creator<DateHolder>() {

        @Override
        public DateHolder[] newArray(int size) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public DateHolder createFromParcel(Parcel source) {
            // TODO Auto-generated method stub
            return new DateHolder(source.readBundle());
        }
    };

    public static String getMonthName(int i){
        String dates[] = {"January","February","March","April","May","June","July","August","September","October","November","December"};
        return dates[i];
    }

    public static String getNowDate() {
        Date d = new Date();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        return df.format(d);
    }
}
