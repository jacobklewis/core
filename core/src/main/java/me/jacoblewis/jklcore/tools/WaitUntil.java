package me.jacoblewis.jklcore.tools;

/**
 * Created by Jacob on 9/16/2017.
 */

public interface WaitUntil<T> {
    T object();
    void action(T obj);
}
