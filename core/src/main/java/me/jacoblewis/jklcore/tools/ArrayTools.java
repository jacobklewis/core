package me.jacoblewis.jklcore.tools;

import android.content.Context;
import android.support.annotation.Nullable;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Jacob Lewis on 5/29/2017.
 */

public class ArrayTools {

    @Nullable
    public static <T> String[] convertToStringArray(Class<T> clazz, ArrayList<T> mCons, String field) {
        String temp[] = new String[mCons.size()];
        Field f;
        try {
            f = clazz.getField(field);
        } catch (NoSuchFieldException e) {
            return null;
        }
        for(int i=0; i<mCons.size(); i++) {
            try {
                temp[i] = f.get(mCons.get(i)).toString();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return temp;
    }

    public static <T> List<Map<String,String>> convertToStringMap(Class<T> clazz, ArrayList<T> mCons, String key, String[] fields) {
        return convertToStringMap(clazz, mCons, key, fields, null);
    }

    public static <T> List<Map<String,String>> convertToStringMap(Class<T> clazz, ArrayList<T> mCons, String key, String[] fields, String blank) {
        List<Map<String,String>> temp = new ArrayList<>();
        List<Field> allFields = Arrays.asList(clazz.getFields());
        if (blank!=null) {
            Map<String, String> tmap = new HashMap<>();
            tmap.put(key, blank);
            temp.add(tmap);
        }
        for(int i=0; i<mCons.size(); i++) {
            Map<String, String> tmap = new HashMap<>();
            String a = "";
            for (String f : fields) {
                Field field = getFieldByName(allFields, f);
                if (field != null){
                    if (!a.equals("")) a += " ";
                    try {
                        a += field.get(mCons.get(i)).toString();
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }else {
                    a += f;
                }
            }
            tmap.put(key, a);
            temp.add(tmap);
        }
        return temp;
    }

    @Nullable
    public static Field getFieldByName(List<Field> fields, String name) {
        for (Field field: fields) {
            if (field.getName().equals(name))
                return field;
        }
        return null;
    }

    static public <T> int getIndexFromField(Class<T> clazz, ArrayList<T> arrayList, String field, int comparison) {
        Field f;
        try {
            f = clazz.getField(field);
        } catch (NoSuchFieldException e) {
            return -1;
        }
        for (T obj: arrayList) {
            try {
                if (f.getInt(obj) == comparison) return arrayList.indexOf(obj);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return -1;
    }

    @Nullable
    static public <T> T getObjFromField(Class<T> clazz, ArrayList<T> arrayList, String field, int comparison) {
        Field f;
        try {
            f = clazz.getField(field);
        } catch (NoSuchFieldException e) {
            return null;
        }
        for (T obj: arrayList) {
            try {
                if (f.getInt(obj) == comparison) return obj;
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public static List<Map<String,String>> convertToStringMap(Context context, int id, String key) {
        return convertToStringMap(context, id, key, null);
    }

    public static List<Map<String,String>> convertToStringMap(Context context, int id, String key, String blank) {
        List<Map<String,String>> temp = new ArrayList<>();
        if (blank!=null) {
            Map<String, String> tmap = new HashMap<>();
            tmap.put(key, blank);
            temp.add(tmap);
        }
        String names[] = context.getResources().getStringArray(id);
        for (String name : names) {
            Map<String, String> tmap = new HashMap<>();
            tmap.put(key, name);
            temp.add(tmap);
        }
        return temp;
    }

    static public <T, J> HashMap<T, J> createHash(T key1, J val1, Object ...keyVals) {
        HashMap<T, J> mHash = new HashMap<>();
        mHash.put(key1, val1);
        if (keyVals.length%2!=0)
            throw new AssertionError("keyVals must be in pairs");
        for (int i = 0; i < keyVals.length; i+=2) {
            mHash.put((T) keyVals[i], (J) keyVals[i + 1]);
        }

        return mHash;
    }

    public static <T> T[] convertToArray(Class<T> clazz, ArrayList<T> arrayList) {
        @SuppressWarnings("unchecked")
        T temp[] = (T[]) Array.newInstance(clazz,arrayList.size());
        for (int i = 0; i < arrayList.size(); i++) {
            temp[i] = arrayList.get(i);
        }
        return temp;
    }

    public static <T> T[] concat (T[] a, T[] b) {
        int aLen = a.length;
        int bLen = b.length;

        @SuppressWarnings("unchecked")
        T[] c = (T[]) Array.newInstance(a.getClass().getComponentType(), aLen+bLen);
        System.arraycopy(a, 0, c, 0, aLen);
        System.arraycopy(b, 0, c, aLen, bLen);

        return c;
    }
}
