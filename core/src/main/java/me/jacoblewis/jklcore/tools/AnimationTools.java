package me.jacoblewis.jklcore.tools;

import android.animation.ObjectAnimator;
import android.graphics.Point;
import android.graphics.Rect;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Interpolator;

/**
 * Created by Jacob Lewis on 5/29/2017.
 */

public class AnimationTools {
    public static ObjectAnimator animateViewMargins(View view, Interpolator interpolator, long time, Integer left, Integer top, Integer right, Integer bottom) {
        ViewGroup.MarginLayoutParams lp = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        Rect oldMarg = new Rect(lp.leftMargin,lp.topMargin,lp.rightMargin,lp.bottomMargin);
        Rect newMarg = new Rect( left==null?oldMarg.left:left ,
                top==null?oldMarg.top:top,
                right==null?oldMarg.right:right,
                bottom==null?oldMarg.bottom:bottom);

        ObjectAnimator marg = ObjectAnimator.ofFloat(new MarginAni(view,oldMarg,newMarg),"val",0f,1f);
        marg.setInterpolator(interpolator);
        marg.setDuration(time);
        return marg;
    }
    public static ObjectAnimator animateViewSize(View view, Interpolator interpolator, long time, Integer width, Integer height) {
        ViewGroup.LayoutParams lp = view.getLayoutParams();
        Point oldSize = new Point(lp.width,lp.height);
        Point newSize = new Point(width!=null?width:oldSize.x,height!=null?height:oldSize.y);

        ObjectAnimator sizeA = ObjectAnimator.ofFloat(new SizeAni(view,oldSize,newSize),"val",0f,1f);
        sizeA.setInterpolator(interpolator);
        sizeA.setDuration(time);
        return sizeA;
    }

    // ANIMATION HELPERS
    private static class MarginAni {
        private float val = 0;
        private View view;
        private Rect newMargins,oldMargins;

        public MarginAni(View view, Rect oldMargins, Rect newMargins) {
            this.view = view;
            this.oldMargins = oldMargins;
            this.newMargins = newMargins;
        }
        public void setVal(float marg) {
            this.val = marg;
            ViewGroup.MarginLayoutParams lp = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
            int left = (int) ((newMargins.left-oldMargins.left)*marg + oldMargins.left);
            int top = (int) ((newMargins.top-oldMargins.top)*marg + oldMargins.top);
            int right = (int) ((newMargins.right-oldMargins.right)*marg + oldMargins.right);
            int bottom = (int) ((newMargins.bottom-oldMargins.bottom)*marg + oldMargins.bottom);
            lp.setMargins(left,top,right,bottom);
            view.setLayoutParams(lp);
        }
        public float getVal() {
            return val;
        }
    }
    private static class SizeAni {
        private float val = 0;
        private View view;
        private Point newSize,oldSize;

        public SizeAni(View view, Point oldSize, Point newSize) {
            this.view = view;
            this.oldSize = oldSize;
            this.newSize = newSize;
        }
        public void setVal(float iterate) {
            this.val = iterate;
            ViewGroup.LayoutParams lp = view.getLayoutParams();
            lp.width = (int) ((newSize.x-oldSize.x)*iterate +oldSize.x);
            lp.height = (int) ((newSize.y-oldSize.y)*iterate +oldSize.y);
            view.setLayoutParams(lp);
        }
        public float getVal() {
            return val;
        }
    }
}
