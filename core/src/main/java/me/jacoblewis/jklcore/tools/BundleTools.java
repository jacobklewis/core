package me.jacoblewis.jklcore.tools;

import android.os.Bundle;

import me.jacoblewis.jklcore.Tools;

/**
 * Created by Jacob Lewis on 5/29/2017.
 */

public class BundleTools {

    private static <T> void putInBundle(Bundle bun, String key, T val) {
        switch (Tools.getInstance(null).getJavaType(val.getClass())){
            case BOOLEAN:
                bun.putBoolean(key, (Boolean) val);
                break;
            case BYTE:
                bun.putByte(key, (Byte) val);
                break;
            case SHORT:
                bun.putShort(key, (Short) val);
                break;
            case INTEGER:
                bun.putInt(key, (Integer) val);
                break;
            case LONG:
                bun.putLong(key, (Long) val);
                break;
            case FLOAT:
                bun.putFloat(key, (Float) val);
                break;
            case DOUBLE:
                bun.putDouble(key, (Double) val);
                break;
            case STRING:
                bun.putString(key, (String) val);
                break;
            default:break;
        }
    }

    public static <A> Bundle c(String key1, A val1) {
        Bundle bun = new Bundle();
        putInBundle(bun, key1, val1);
        return bun;
    }
    public static <A,B> Bundle c(String key1, A val1,
                                 String key2, B val2) {
        Bundle bun = c(key1, val1);
        putInBundle(bun, key2, val2);
        return bun;
    }
    public static <A,B,C> Bundle c(String key1, A val1,
                                   String key2, B val2,
                                   String key3, C val3) {
        Bundle bun = c(key1, val1, key2, val2);
        putInBundle(bun, key3, val3);
        return bun;
    }
    public static <A,B,C,D> Bundle c(String key1, A val1,
                                     String key2, B val2,
                                     String key3, C val3,
                                     String key4, D val4) {
        Bundle bun = c(key1, val1, key2, val2, key3, val3);
        putInBundle(bun, key4, val4);
        return bun;
    }
    public static <A,B,C,D,E> Bundle c(String key1, A val1,
                                       String key2, B val2,
                                       String key3, C val3,
                                       String key4, D val4,
                                       String key5, E val5) {
        Bundle bun = c(key1, val1, key2, val2, key3, val3, key4, val4);
        putInBundle(bun, key5, val5);
        return bun;
    }
}
