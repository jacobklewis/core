package me.jacoblewis.jklcore;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Rect;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.Vibrator;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Display;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.EdgeEffect;
import android.widget.ScrollView;
import android.widget.Toast;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import me.jacoblewis.jklcore.tools.IfIs;
import me.jacoblewis.jklcore.tools.WaitUntil;

/**
 * Created by Jacob on 9/16/2017.
 */

public class Tools {
    private Context context;
    public static Tools getInstance(Context context) {
        return new Tools(context);
    }

    private Tools(Context context) {
        this.context = context;
    }

    /* ----- VIEW TOOLS ----- */
    public Rect getViewDimens(View v) {
        int[] co_dims = new int[4]; // 2 coords + 2 dimens
        int[] coords = new int[2];
        v.getLocationOnScreen(coords);
        co_dims[0] = coords[0];
        co_dims[1] = coords[1];
        co_dims[2] = v.getMeasuredWidth();
        co_dims[3] = v.getMeasuredHeight();
        return new Rect(co_dims[0],co_dims[1],co_dims[0]+co_dims[2],co_dims[1]+co_dims[3]);
    }

    public void hideSoftKeyboard(Activity activity) {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager)  activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* ----- VIEW TOOLS ----- */
    public float dp2px(float dp) {
        Resources r = context.getResources();
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics());
    }

    public float px2dp(float px) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return Math.round(px / (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    public Point getDisplaySize(FragmentActivity activity) {
        Rect windowSize = new Rect();
        activity.getWindow().getDecorView().getWindowVisibleDisplayFrame(windowSize);
        return new Point(windowSize.width(), windowSize.height());
    }

    public boolean isScreenWider(FragmentActivity activity, float dp) {
        return px2dp(getDisplaySize(activity).x)>=dp;
    }

    public Point getScreenDimens(Activity activity) {
        Display display = activity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size;
    }

    public int blendColors(int color1, int color2, float ratio) {
        final float inverseRation = 1f - ratio;
        float r = (Color.red(color2) * ratio) + (Color.red(color1) * inverseRation);
        float g = (Color.green(color2) * ratio) + (Color.green(color1) * inverseRation);
        float b = (Color.blue(color2) * ratio) + (Color.blue(color1) * inverseRation);
        return Color.rgb((int) r, (int) g, (int) b);
    }

    public enum JAVA_TYPES {BOOLEAN, INTEGER, LONG, FLOAT, DOUBLE, STRING, BYTE, SHORT}
    public <T> JAVA_TYPES getJavaType(Class<T> clazz) {
        Class[] classes = { boolean.class, Boolean.class,
                int.class, Integer.class,
                long.class, Long.class,
                float.class, Float.class,
                double.class, Double.class,
                byte.class, Byte.class,
                short.class, Short.class,
                String.class};
        switch (Arrays.asList(classes).indexOf(clazz)) {
            case 0:case 1:
                return JAVA_TYPES.BOOLEAN;
            case 2:case 3:
                return JAVA_TYPES.INTEGER;
            case 4:case 5:
                return JAVA_TYPES.LONG;
            case 6:case 7:
                return JAVA_TYPES.FLOAT;
            case 8:case 9:
                return JAVA_TYPES.DOUBLE;
            case 10:case 11:
                return JAVA_TYPES.BYTE;
            case 12:case 13:
                return JAVA_TYPES.SHORT;
            default:
                return JAVA_TYPES.STRING;
        }
    }

    public JAVA_TYPES getJavaType(Field field) {
        return getJavaType(field.getType());
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager conManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = null;
        if (conManager != null) {
            networkInfo = conManager.getActiveNetworkInfo();
        }
        return (networkInfo != null && networkInfo.isConnected());
    }

    public <T> void waitUntilNotNull(final WaitUntil<T> action) {
        if (action.object()==null) {
            Handler h = new Handler();
            h.postDelayed(new Runnable() {
                @Override
                public void run() {
                    waitUntilNotNull(action);
                }
            }, 25);
        }else {
            action.action(action.object());
        }
    }

    /* ----- SYSTEM TOOLS ----- */
    public Vibrator getVibrator() {
        return (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
    }

    // STORAGE HELPERS
    /* Checks if external storage is available for read and write */
    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state);
    }

    /* Checks if external storage is available to at least read */
    public boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state);
    }


    // EDGE GLOW HELPERS
    private static final Class<?> CLASS_SCROLL_VIEW = ScrollView.class;
    private static final Field SCROLL_VIEW_FIELD_EDGE_GLOW_TOP;
    private static final Field SCROLL_VIEW_FIELD_EDGE_GLOW_BOTTOM;

    private static final Class<?> CLASS_LIST_VIEW = AbsListView.class;
    private static final Field LIST_VIEW_FIELD_EDGE_GLOW_TOP;
    private static final Field LIST_VIEW_FIELD_EDGE_GLOW_BOTTOM;

    static {
        Field edgeGlowTop = null, edgeGlowBottom = null;

        for (Field f : CLASS_SCROLL_VIEW.getDeclaredFields()) {
            switch (f.getName()) {
                case "mEdgeGlowTop":
                    f.setAccessible(true);
                    edgeGlowTop = f;
                    break;
                case "mEdgeGlowBottom":
                    f.setAccessible(true);
                    edgeGlowBottom = f;
                    break;
            }
        }

        SCROLL_VIEW_FIELD_EDGE_GLOW_TOP = edgeGlowTop;
        SCROLL_VIEW_FIELD_EDGE_GLOW_BOTTOM = edgeGlowBottom;

        for (Field f : CLASS_LIST_VIEW.getDeclaredFields()) {
            switch (f.getName()) {
                case "mEdgeGlowTop":
                    f.setAccessible(true);
                    edgeGlowTop = f;
                    break;
                case "mEdgeGlowBottom":
                    f.setAccessible(true);
                    edgeGlowBottom = f;
                    break;
            }
        }

        LIST_VIEW_FIELD_EDGE_GLOW_TOP = edgeGlowTop;
        LIST_VIEW_FIELD_EDGE_GLOW_BOTTOM = edgeGlowBottom;
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void setEdgeGlowColor(AbsListView listView, int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            try {
                EdgeEffect ee;
                ee = (EdgeEffect) LIST_VIEW_FIELD_EDGE_GLOW_TOP.get(listView);
                ee.setColor(color);
                ee = (EdgeEffect) LIST_VIEW_FIELD_EDGE_GLOW_BOTTOM.get(listView);
                ee.setColor(color);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void setEdgeGlowColor(ScrollView scrollView, int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            try {
                EdgeEffect ee;
                ee = (EdgeEffect) SCROLL_VIEW_FIELD_EDGE_GLOW_TOP.get(scrollView);
                ee.setColor(color);
                ee = (EdgeEffect) SCROLL_VIEW_FIELD_EDGE_GLOW_BOTTOM.get(scrollView);
                ee.setColor(color);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void setEdgeGlowColor(final RecyclerView recyclerView, final int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            try {
                final Class<?> clazz = RecyclerView.class;
                for (final String name : new String[] {"ensureTopGlow", "ensureBottomGlow"}) {
                    Method method = clazz.getDeclaredMethod(name);
                    method.setAccessible(true);
                    method.invoke(recyclerView);
                }
                for (final String name : new String[] {"mTopGlow", "mBottomGlow"}) {
                    final Field field = clazz.getDeclaredField(name);
                    field.setAccessible(true);
                    final Object edge = field.get(recyclerView); // android.support.v4.widget.EdgeEffectCompat
                    final Field fEdgeEffect = edge.getClass().getDeclaredField("mEdgeEffect");
                    fEdgeEffect.setAccessible(true);
                    ((EdgeEffect) fEdgeEffect.get(edge)).setColor(color);
                }
            } catch (final Exception ignored) {}
        }
    }

    public Boolean checkValidIntent(Context context, Intent intent) {
        if (context.getPackageManager().resolveActivity(intent,0) != null)
            return true;
        Toast.makeText(context, "Unable to find compatible app", Toast.LENGTH_LONG).show();
        return false;
    }

    public void callNumber(String number) {
        Intent callwork = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + number));
        if (checkValidIntent(context, callwork))
            context.startActivity(callwork);
    }

    public void sendEmail(String email) {
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto",email, null));
        if (checkValidIntent(context, emailIntent))
            context.startActivity(Intent.createChooser(emailIntent, "Send email..."));
    }

    public void toWeb(String url) {
        Intent toWeb = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        if (checkValidIntent(context, toWeb))
            context.startActivity(toWeb);
    }

    // Back Out of App
    private int bkaCounter = 1;
    private Handler h;
    private Toast t;
    public Boolean backoutOfApp(final String message) {
        if(bkaCounter>=2) {
            bkaCounter = 1;
            return true;
        }else {
            if(h!=null)
                h.removeCallbacksAndMessages(null);
            h = new Handler();
            h.postDelayed(new Runnable() {
                @Override
                public void run() {
                    bkaCounter = 1;
                }
            },2000);

            if(t!=null)
                t.cancel();
            t = Toast.makeText(context,message, Toast.LENGTH_SHORT);
            t.show();
            bkaCounter++;
            return false;
        }
    }

    public String getHTTPFormat(String url) {
        int start = url.indexOf("http://");
        int mid = url.indexOf("www");
        if (start==-1) {
            return "http://" + (mid==-1?("www." + url): url);
        }else {
            return url;
        }
    }

    public String getNumberFormat(String number) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = number.length()-3; i > 0; i -= 3) {
            stringBuilder.insert(0, number.substring(i, i+3));
            stringBuilder.insert(0,",");
        }
        int offset = number.length()%3;
        stringBuilder.insert(0, number.substring(0, offset == 0 ? 3 : offset));
        return stringBuilder.toString();
    }

    public String getPhoneFormat(String number, Boolean style) {
        StringBuilder newNum = new StringBuilder();

        if(number.length()>1 && number.charAt(0) == '1')
            number = number.substring(1);

        for(int i=0; i<number.length(); i++)
        {
            if(number.charAt(i)=='0'||number.charAt(i)=='1'||number.charAt(i)=='2'||
                    number.charAt(i)=='3'||number.charAt(i)=='4'||number.charAt(i)=='5'||
                    number.charAt(i)=='6'||number.charAt(i)=='7'||number.charAt(i)=='8'||
                    number.charAt(i)=='9'){

                newNum.append(number.charAt(i));
            }
        }
        if(style) {
            if (newNum.length() > 7)
                newNum = new StringBuilder("(" + newNum.substring(0, 3) + ") " + newNum.substring(3, 6) + "-" + newNum.substring(6));
            else if (newNum.length() > 3)
                newNum = new StringBuilder(newNum.substring(0, 3) + "-" + newNum.substring(3));
        }

        return newNum.toString();
    }
    public String getPhoneFormat(String number)
    {
        return getPhoneFormat(number,true);
    }

    public void showConfirmDialog(DialogInterface.OnClickListener onClick,
                                  String message,
                                  String confirmText,
                                  String cancelText) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
        builder1.setMessage(message)
                .setPositiveButton(confirmText, onClick).setNegativeButton(cancelText, null);
        builder1.create().show();
    }

    public void showAlertDialog(String title, String message) {
        android.app.AlertDialog ad = new android.app.AlertDialog.Builder(context).setTitle(title).setMessage(message).setPositiveButton("OK",null).create();
        ad.show();
    }

    // Adds a key to ifisSet when setCheck is called.
    // Once checkIf is called, if the key exists, it is removed and the ifIs delegate is called.
    private Set<String> ifisSet = new HashSet<>();
    public void setCheck(String key) {
        ifisSet.add(key);
    }
    public void checkIf(String key, IfIs ifIs) {
        if (ifisSet.contains(key))
            ifIs.doThis();
        ifisSet.remove(key);
    }

}
